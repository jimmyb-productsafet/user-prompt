var readline = require('readline');
var chalk = require('chalk');
var rl;

var questionLoop = function(prompt, required, defaultAnswer, callback) {
	rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout
	});
	rl.question(prompt, function(answer) {
		rl.close();
		if(answer) return callback(answer);
		if(!answer && defaultAnswer) return callback(defaultAnswer);
		if(!answer && required) return questionLoop('', required, defaultAnswer);
		return callback(null);
	});
};

module.exports = {
	question: function(prompt, required, defaultAnswer) {
		if(typeof defaultAnswer == typeof function() {}) {
			callback = defaultAnswer;
			defaultAnswer = null;
		}
		if(typeof required == typeof 'string') {
			defaultAnswer = required;
			required = false;
		}
		if(typeof required == typeof function() {}) {
			callback = required;
			defaultAnswer = null;
			required = false;
		}
		if(required) prompt = chalk.red('* ') + prompt + ":";
		if(defaultAnswer) prompt += ' (' + defaultAnswer + ')';
		prompt += ' ';
		questionLoop(prompt, required, defaultAnswer, callback);
	}
};
